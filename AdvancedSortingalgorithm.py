import numpy as np



def sorting_algorithm(array, index,descending=True):

    # We create 3 subarrays: ints (or strings that could be converted to int, strings and garbage/other)
    # We then sort them seperately and merge them once more.

    num_rows, num_cols = array.shape

    rows_to_keep_string = []
    rows_to_keep_int = []
    rows_to_delete = []

    # todo add dates.
    for i in range(0,num_rows):

        if (isinstance(array[i][index], str)):

            #If we can convert it to an int:
            try:
                array[i][index] = int(array[i][index])
                rows_to_keep_int.append(i)
                #print(array[i][index], ' could be converted to int')
            except:
                rows_to_keep_string.append(i)
                #print(array[i][index], ' INCONVERTIBLE STRING')

        elif (isinstance(array[i][index], int)):
            #print('was an int')
            rows_to_keep_int.append(i)

        else:
            #print('was another data type')
            # This is a bad way of doing things as the record ID will eventually be different.
            rows_to_delete.append(i)

    #print('rows to delete: ', rows_to_delete)

    # Isolate only strongs, only bad values, only ints.

    # Stores the values to delete

    array_good_strings = np.delete(array,rows_to_delete + rows_to_keep_int,0)
    array_good_ints= np.delete(array, rows_to_delete + rows_to_keep_string, 0)
    array_bad_values = np.delete(array,rows_to_keep_int + rows_to_keep_string,0)

    #print('strings: ', array_good_strings)
    #print('ints: ', array_good_ints)
    #print('crap: ', array_bad_values)
    #print('dates: ', 'TODO !!!!!!!!!!')

    if descending == True:
        array_good_strings_sorted = array_good_strings[array_good_strings[:, index].argsort()[::-1]]
        array_good_ints_sorted = array_good_ints[array_good_ints[:, index].argsort()[::-1]]

    elif descending == False:
        array_good_strings_sorted = array_good_strings[array_good_strings[:, index].argsort()]
        array_good_ints_sorted = array_good_ints[array_good_ints[:, index].argsort()]

    #The 3rd argument is the axis.
    sorted_Array = np.concatenate((array_good_ints_sorted, array_good_strings_sorted, array_bad_values),axis=0)

    return sorted_Array


#### ---------------------------------------------------------------------------------------
# list1 = ['2020-05-12 FAIL','x','y','z']
# list2 = ['2020-04-10 SUCCESS','q','b','c']
# list3 = ['2019-12-05 FAIL','x','y','x']
# list4 = ['2019-01-02','1','2','3']
# list5 = [5,None, [1,1,3,], 4]
# list6 = [99,101,220,323]
# #
# list7 = [list1, list2, list3, list4, list5, list6]
# #
# array1 = np.array(list7)
#
# #
# # print('array: \n ', array1)
# #
# #
# # #TODO this is the correct syntax!!!!!!!!!!!
#
# # array2 = array[array[:,2].argsort()[::-1]]
# # print(array2)
# # array2 = array[array[:,2].argsort()]
# # print(array2)
#
# array2 = sorting_algorithm(array1,0, descending=True)
# print('array 2: \n', array2)

